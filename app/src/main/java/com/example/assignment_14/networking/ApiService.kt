package com.example.assignment_14.networking

import com.example.assignment_14.users.models.User
import com.example.assignment_14.utils.Constants.Companion.USERS
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET(USERS)
    suspend fun getUsers(
        @Query("page") page: Int,
    ): Response<User>

}


//    @Headers("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")