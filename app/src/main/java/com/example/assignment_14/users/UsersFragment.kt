package com.example.assignment_14.users

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment_14.abstracts.BaseFragment
import com.example.assignment_14.databinding.FragmentUsersBinding

class UsersFragment : BaseFragment<FragmentUsersBinding>(FragmentUsersBinding::inflate) {

    private lateinit var usersAdapter: UsersAdapter
    private val viewModel: UsersViewModel by viewModels()

    override fun init() {
        initRV()
//        listeners()
//        observes()
//        viewModel.loadMovies()
    }

    private fun listeners() {
        binding.swipeStatementFragment.setOnRefreshListener {
            viewModel.loadUsers()
        }
    }

    private fun initRV() {
        binding.rvUsers.apply {
            usersAdapter = UsersAdapter()
            adapter = usersAdapter
            adapter = usersAdapter.withLoadStateHeaderAndFooter(
                header = UsersLoadingAdapter(usersAdapter),
                footer = UsersLoadingAdapter(usersAdapter)
            )
            layoutManager = LinearLayoutManager(view?.context)
        }
        lifecycleScope.launchWhenCreated {
            viewModel.loadUsers().observe(viewLifecycleOwner, {
                usersAdapter.submitData(lifecycle, it)
            })
        }

//        binding.rvUsers.adapter.with(
//            UsersLoadingAdapter{adapter.retry()}
//        )
//        usersAdapter.submitData()
    }
//
//    private fun observes() {
//        viewModel.getUsers().observe(viewLifecycleOwner, {
//            when (it) {
//                is Resource.Loading -> {
//                    binding.swipeStatementFragment.refreshing(true)
//                }
//                is Resource.Success -> {
//                    binding.swipeStatementFragment.refreshing(false)
//                    moviesAdapter.setData(it.data!!.data!!)
//                }
//                is Resource.Error -> {
//                    binding.swipeStatementFragment.refreshing(true)
//                }
//            }
//        })
//    }
}