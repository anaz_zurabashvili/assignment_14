package com.example.assignment_14.users

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.assignment_14.networking.NetworkClient
import com.example.assignment_14.users.models.User
import kotlinx.coroutines.delay
import java.lang.Exception

class UsersPagingSource : PagingSource<Int, User.Data>() {
    override fun getRefreshKey(state: PagingState<Int, User.Data>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, User.Data> {
        return try {
            val pageNumber = params.key ?: 1
            val response = NetworkClient.apiService.getUsers(pageNumber)
            val body = response.body()
            if (response.isSuccessful && body != null) {
                LoadResult.Page(
                    data = body.data!!,
                    prevKey = if (pageNumber == 1) null else pageNumber - 1,
                    nextKey = if (body.totalPages!! > pageNumber) pageNumber + 1 else null
                )
            } else {
                LoadResult.Error(Throwable())
//                Resource.Error(message = response.message().toString())
            }

        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}