package com.example.assignment_14.users

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*

class UsersViewModel : ViewModel() {

    fun loadUsers() =
        Pager(
            config = PagingConfig(pageSize = 1),
            pagingSourceFactory = { UsersPagingSource() }).liveData.cachedIn(viewModelScope)


//        ??.postValue(Resource.Loading())
//        try {
//            val result = NetworkClient.service.getUsers(1)
//            val body = result.body()
//            ??.postValue(
//            if (result.isSuccessful && body != null)
//                Resource.Success(body)
//            else
//                Resource.Error(message = result.message().toString())
//            )
//        } catch (e: Exception) {
//            ??.postValue(Resource.Error(message = e.toString()))
//        }
}
