package com.example.assignment_14.utils

class Constants {
    companion object{
        const val BASE_URL = "https://reqres.in/api/"
        const val USERS = "users"
    }
}