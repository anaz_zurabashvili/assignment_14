package com.example.assignment_14.utils

import android.view.View
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.example.assignment_14.R
import com.google.android.material.snackbar.Snackbar


fun View.showSnackBar(title: String) =
    Snackbar.make(this, title, Snackbar.LENGTH_SHORT).show()

fun View.gone() = View.GONE.also { visibility = it }

fun View.visible() = View.VISIBLE.also { visibility = it }

fun SwipeRefreshLayout.refreshing(boolean:Boolean) {
    isRefreshing = boolean
}

fun ImageView.setImageUrl(url: String?) {
    if (!url.isNullOrEmpty())
        Glide.with(context).load(url).placeholder(R.drawable.ic_launcher_foreground).into(this)
    else
        setImageResource(R.drawable.ic_launcher_foreground)
}
