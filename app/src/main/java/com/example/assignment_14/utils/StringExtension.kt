package com.example.assignment_14.utils

import kotlin.math.pow

fun Int.pow(): Int  = this.toDouble().pow(2.0).toInt()
